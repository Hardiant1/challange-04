class Component {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
    withDriver,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
    this.withDriver = withDriver
    if (this.constructor === Component) {
      throw new Error("...")
    }
  }

  render() {
    return `
    <div class="col-sm-4 col-sm-4 col-sm-4 mt-3">
      <div class="card" >
        <div style ="margin: 20px 0px 20px 20px"><center>
          <img src="${this.image}" alt="${this.manufacture}" height="160" width="300">
        <center></div>
        <div class="card-body">
          <p>${this.model} / ${this.plate}</p>
          <p><strong>Rp. ${this.rentPerDay.toLocaleString()} / hari </strong></p>
          <p style="height:75px">${this.description}</p>
          <div class="row">
            <div class="col-sm-8"><p><img src="./style/image/user-icon.svg">&nbsp;${this.capacity} orang</p></div>
            <div class="col-sm-8"><p><img src="./style/image/setting-icon.svg">&nbsp;${this.transmission}</p></div>
            <div class="col-sm-8"><p><img src="./style/image/calendar-icon.svg">&nbsp;Tahun&nbsp;${this.year}</p></div>
          </div>
          <div class="btn-success btn-lg"><center>Pilih Mobil</center></div>
        </div>
      </div>
    </div>
    `;
  }
}

class Car extends Component {

}